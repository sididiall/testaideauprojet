<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'aideprojet');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_4~jfON$Zad;*0SEJI.R.mTS,1]zs:Nk-A*{(yejkYzVc{m}3*(l8x&)`Ir=iAY@');
define('SECURE_AUTH_KEY',  ' 7}jm_=D[J=$e.:usN_}w:rkuTv^655X)>OpEB^+TM`A0T>sda.R=M?oF@zp}{yG');
define('LOGGED_IN_KEY',    'u:ij>|]L`jSr5#]ZRG6?W+m1(y>lI]o8Wh0~O*Am^&2p>4@bnPzoHSFdIzC%NZ[-');
define('NONCE_KEY',        '4DP?&rMRj)@JJRrXn}V.(9!aREqL}Z-{U5>$+x;UY%E$;SJqsIHa}i]u8=hb4R;N');
define('AUTH_SALT',        'mM?H4~7xf0Ts(=qs?V1EcFZ^3Inlb0S7u]<Mh^pk`!:~raa2=jmOY &m<N,{Zm?6');
define('SECURE_AUTH_SALT', ',<jbKEM$lmUJKQZ%!+hhMHH8?<!Fz2F+m3k(w+[2TjStg87>eeu,{1t(Z-BOW~9M');
define('LOGGED_IN_SALT',   'SZ n9_>J9yOxyAr_A,qPIwo(m%mm~/0UdsjO%Nb|joYyd$RGAGXDo2^wd%?a~~h3');
define('NONCE_SALT',       ' 95{* A&fd]~_v0Gqt1<C3*40!cy^lr)-H9+fY8Tr[SwwF}x!$gKV/Z4f#TY>6jk');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

<?php
/**
 * Plugin Name: YABAWT-GridSystem
 * Plugin URI:  http://www.google.com
 * Description: GridSystem
 * Author:      Yabawt
 * Author URI:  http://www.google.com
 * Version:     1.0.0
 * Text Domain: yabawt
 * Domain Path: /lang/
 * License:     GPLv2 or later (license.txt)
 */

function registerGrid(){
	wp_enqueue_style('yabawt-grid', plugins_url().'/ybt-grid/css/grid.css');
}
add_action( 'wp_enqueue_scripts', 'registerGrid' );


function row_func( $atts , $content){
	return '<div class="row">'.do_shortcode($content).'</div>';
}
add_shortcode( 'row', 'row_func' );



function col_func( $atts , $content){
	 $a = shortcode_atts( array(
	 	'xs' => '0',
	 	'sm' => '0',
        'md' => '0',
        'lg' => '0',
        
		'off_xs' => '0',
	 	'off_sm' => '0',
        'off_md' => '0',
        'off_lg' => '0'
    ), $atts );
	
	$classes = array();
	
	$profiles = array('xs', 'sm', 'md', 'lg');
	
	foreach ($profiles as $key => $profile) {
		if($a[$profile] != '0')
			$classes[] = 'col-'.$profile.'-'.$a[$profile];
		
		if($a['off_'.$profile] != '0')
			$classes[] = 'col-'.$profile.'-offset-'.$a[$profile];
	}
	
	return '<div class="'.implode(' ', $classes).'">'.do_shortcode($content).'</div>';
}
add_shortcode( 'col', 'col_func' );
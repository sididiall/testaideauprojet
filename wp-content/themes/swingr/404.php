<?php
get_header();
?>
<div class="banner banner-404" style="background-image: url(<?php echo get_theme_directory()?>/assets/img/background-img.jpg);">
    <div>
        <div class="frame">
            <div class="title">Oups !</div>
            <div class="little-shield-holder">
                <div class="ybt-line"></div>
                <img class="imglogo" src="<?php echo get_theme_directory()?>/assets/img/as-shield-mini-color.png" alt="Audit Sécurité">
                <div class="ybt-line"></div>
            </div>
            <div class="subtitle">Impossible de trouver la page demandée</div>
        </div>
    </div>
</div>
<div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">
        <div class="m-t-40">&nbsp;</div>

        <h1>Oups, impossible de trouver la page demandée</h1>
        <p>
            Si vous avez ecrit cette adresse, assurez-vous que cette dernière est bien valide.
        </p>		
    </div>
</div><!-- #content -->
</div><!-- #primary -->


<?php get_footer(); ?>
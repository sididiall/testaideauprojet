<?php

// Exit if accessed directly
if (!defined('ABSPATH'))
    exit;

function asset_url($path){
    if (file_exists(get_stylesheet_directory() . '/assets/'.$path )) {
        $url = get_stylesheet_directory_uri();
    }else{
        $url = get_template_directory_uri(); 
    }
    return $url . '/assets/'.$path;
}

function e_asset_url($path){
    echo asset_url($path);
}    

function root_asset_url($path){    
    return get_template_directory_uri() . '/assets/'.$path;
}
// retourne l'adresse relative du thème enfant 
function get_theme_directory(){
	return dirname( get_bloginfo('stylesheet_url') );	
}

remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );

require_once 'inc/config-visual-composer.php';
require_once 'inc/config-shortcodes.php';
require_once 'inc/config-styles.php';
require_once 'inc/config-menus.php';    
require_once 'inc/config-sidebar.php';    
require_once 'inc/config-widget.php';    
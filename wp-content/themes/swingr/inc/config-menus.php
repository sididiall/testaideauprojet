<?php

function setting_features() {
	add_theme_support("menus");
}
add_action('after_setup_theme', 'setting_features');

function register_menu() {
	register_nav_menu('header-menu', __('Header Menu'));
	register_nav_menu('footer-menu', __('Footer Menu'));
}
add_action('init', 'register_menu');

function convert_menu_seps( $items, $args ) {
	if($args->theme_location == 'header-menu'){
		$items = str_replace(" &#8211; ", "<i></i>", $items);
		$items = str_replace(" - ", "<i></i>", $items);
	}
	return $items;
}
add_filter('wp_nav_menu_items','convert_menu_seps', 10, 2);


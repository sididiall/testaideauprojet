<?php

function registerAuditSecuStyles(){
	global $post;
	/*RESET CSS*/
        wp_enqueue_style('reset', get_theme_directory().'/assets/css/reset.css');


        wp_enqueue_style('sd-flex', get_theme_directory().'/assets/css/flex-system.css');
        wp_enqueue_style('sd-footer', get_theme_directory().'/assets/css/footer.css');
        wp_enqueue_style('sd-header', get_theme_directory().'/assets/css/header.css');
        wp_enqueue_style('sd-home', get_theme_directory().'/assets/css/home.css');
        wp_enqueue_style('sd-layout', get_theme_directory().'/assets/css/layout.css');
        wp_enqueue_style('sd-sidebar', get_theme_directory().'/assets/css/sidebar.css');
       
        
        
}
add_action( 'wp_enqueue_scripts', 'registerAuditSecuStyles' );

<?php

add_action('vc_before_init', 'vc_shortcodes');

function vc_shortcodes() {
    include 'vc/vc_section.php';
    include 'vc/vc_content.php';
    include 'vc/vc_skills.php';
    include 'vc/vc_content_img.php';

}

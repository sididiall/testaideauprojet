<?php

function render_content_title_func($atts, $content = "null") {
    $input = shortcode_atts(array(null), $atts);

    ob_start();
    ?>
    <section class="content-section">
        <span class="header-content">Découverte</span>
        <span class="header-content">Ma recherche</span>

        <form>
            <input type="search" id="search" name="q" placeholder="Recherche">
        </form>

        <div class="menu">
            <ul>
                <li>Nouveau</li>
                <li>En ligne</li>
                <li>On fire</li>    
            </ul>
        </div>

    </section>

    <?php

    return ob_get_clean();
}

add_shortcode('contenttitle', 'render_content_title_func');

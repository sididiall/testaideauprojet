<?php
vc_map(array(
    'name' => __('Contenu'),
    'base' => 'vc_title_section',
    'description' => __('Titre de Section'),
    'content_element' => true,
    'category' => 'Custom',
    'js_view' => 'VcColumnView',    
    'params' => array(
        array(
            'type' => 'textfield',
            'heading' => __('Titre de cotenu', 'Custom'),
            'param_name' => 'title'
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __('Couleur de fond titre', 'Custom'),
            'param_name' => 'title_color'
        ),
        array(
            'type' => 'checkbox',
            'heading' => __('Choix Image', 'Custom'),
            'param_name' => 'img_choice',
            'description' => __('Voulez-vous ajouter une image ', 'Custom'),
            "value" => true,
            'admin_label' => true
        ),
        array(
            'type' => 'attach_image',
            'heading' => __('Image', 'Custom'),
            'param_name' => 'img',
            'dependency' => array(
                'element' => 'img_choice',
                'value' => 'true',
            )
        )
    )
));

class WPBakeryShortCode_vc_content extends WPBakeryShortCode{

    protected function content($atts, $content = null) {
        extract(shortcode_atts(array(
            'title' => '',
            'title_color' => '',
            'img_choice' => '',
            'img' => '',
            'color' => '',
            'fullwidth' => '',
                        ), $atts));
        ob_start();

        $parent_class = 'custom-section ';

        if ($fullwidth == false) {
            $parent_class .= 'container';
        }
        //2 conditions à verifier dans la mm condition, la table de verité ne fonctionne dans ce cas que si seulement l'un des 2 cas sont vrai ou les 2.
        if ($shadow_top == true || $shadow_bottom == true) {
            $parent_class .= ' custom-section-shadow '; // Class permettant la configuration du Z-INDEX
        }
        //2 conditions à verifier dans la mm condition, la table de verité ne fonctionne dans ce cas que si seulement les des 2 cas sont vrai 
        $content_class = '';
        if ($container == true && $fullwidth == true) {
            $content_class = 'container';
        }
        $parent_style = '';

        if (!empty($background_img) && !empty($color)) { // Détermine si une variable est définie et est différente de NULL.
            $image_attributes = wp_get_attachment_image_src($background_img, 'fullscreen'); // returns an array
            $bg_url = $image_attributes[0];
            $parent_style .= 'background: ' . $color . ' url(' . $bg_url . '); ';
        } else {
            if (!empty($background_img)) { // Détermine si une variable est définie et est différente de NULL.
                $image_attributes = wp_get_attachment_image_src($background_img, 'fullscreen'); // returns an array
                $bg_url = $image_attributes[0];
                $parent_style .= 'background-image: url(' . $bg_url . '); ';
            }
            $color_bg = '';
            if (!empty($color)) {
                $color_bg = ' background-color: ' . $color . ';';
            }
        }
        
        if($bg_position_status == true){
            $parent_style .= ' background-position : '. $background_position_x . ' ' . $background_position_y . ';' ;
        }
        
        // Paramétrage de la disposition de l'ombre
        if ($shadow_top == true && $shadow_bottom == true) {
            $parent_style .= ' box-shadow: 0 4px 7px ' . $color_shadow_bottom . ',0 -2px 5px ' . $color_shadow_top . '; ';
        } else if ($shadow_top == true) {
            $parent_style .= ' box-shadow : 0 -5px 5px ' . $color_shadow_top . '; ';
        } else if ($shadow_bottom == true) {
            $parent_style .= ' box-shadow: 0 5px 5px ' . $color_shadow_bottom . '; ';
        }



        $padding_css = '';
        if (!empty($pad_t)) {
            $padding_css .= ' padding-top : ' . $pad_t . '; ';
        }
        if (!empty($pad_b)) {
            $padding_css .= ' padding-bottom : ' . $pad_b . '; ';
        }
        if ($has_border == true) {
            $parent_style .= ' border :' . $border_style . '; ';
        }
        $linkObj = '';
        if (isset($link))
            $linkObj = $link;
        $linkObj = ( '||' === $linkObj ) ? '' : $linkObj;
        $linkObj = vc_build_link($linkObj);
        $a_href = '';
        if (strlen($linkObj['url']) > 0) {
            $use_link = true;
            $a_href = $linkObj['url'];
            $a_title = $linkObj['title'];
            $a_target = $linkObj['target'];
        }
        ?>

        <div class="<?php echo $parent_class; ?>" style="<?php echo $parent_style; ?> background-repeat-y: no-repeat; ">             
            <?php if (!empty($a_href)) { ?>
                <a href="<?php echo $a_href ?>">   
                <?php } ?>
                <div class="mask" style="<?php echo $color_bg . $padding_css; ?>">                       
                    <div class="<?php echo $content_class . ' ' . $el_class; ?>">
                        <?php echo do_shortcode($content); ?> 
                    </div>
                </div>                
                <?php if (!empty($a_href)) { ?>
                </a>
            <?php } ?>
        </div> 

        <?php
        $result = ob_get_clean();
        return do_shortcode($result);
    }

}
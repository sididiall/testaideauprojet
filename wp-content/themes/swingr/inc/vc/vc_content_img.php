<?php
vc_map(array(
    'name' => __('Contenu avec image'),
    'base' => 'vc_content_img',
    'description' => __('Contenu avec image'),
    'content_element' => true,
    'category' => 'Custom',
    'js_view' => 'VcColumnView',
    'params' => array(
        array(
            'type' => 'attach_image',
            'heading' => __('Image', 'Custom'),
            'param_name' => 'img',
        ),
        array(
            'type' => 'textfield',
            'heading' => __('Titre de cotenu', 'Custom'),
            'param_name' => 'title'
        ),
        array(
            'type' => 'textfield',
            'heading' => __('Sous-titre', 'Custom'),
            'param_name' => 'subtitle'
        ),
        array(
            'type' => 'textfield',
            'heading' => __('nombre de like', 'Custom'),
            'param_name' => 'nb_like'
        ),
    )
));

class WPBakeryShortCode_vc_content_img extends WPBakeryShortCode {

    protected function content($atts, $content = null) {
        extract(shortcode_atts(array(
            'img' => '',
            'title' => '',
            'subtitle' => '',
            'nb_like' => '',
                        ), $atts));
        ob_start();

        if (!empty($img)) { // Détermine si une variable est définie et est différente de NULL.
            $image_attributes = wp_get_attachment_image_src($img, 'fullscreen'); // returns an array
            $bg_url = $image_attributes[0];
            $parent_style .= 'background: url(' . $bg_url . '); ';
        }
        ?>

        <div class="image-section" style="<?php echo $parent_style; ?> background-repeat-y: no-repeat;">             
            <div class="content-section">
                <div class="title"><?php echo $title; ?> </div>
                <div class="like"><?php echo $nb_like; ?></div>
                <div class="subtitle"><?php echo $subtitle; ?></div>
                
            </div>
        </div> 

        <?php
        $result = ob_get_clean();
        return do_shortcode($result);
    }

}

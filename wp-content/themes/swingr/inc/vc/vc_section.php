<?php
vc_map(array(
    'name' => __('Section'),
    'base' => 'vc_section',
    'description' => __('Conteneur avec image de fond, couleur de fond, ombre...', 'Custom'),
    'content_element' => true,
    'category' => 'Custom',
    'js_view' => 'VcColumnView',
    "is_container" => true,
    'params' => array(
        array(
            'type' => 'attach_image',
            'heading' => __('Image de fond', 'Custom'),
            'param_name' => 'background_img'
        ),
        array(
            'type' => 'checkbox',
            'heading' => __('Choix Positionnement image de fond', 'Custom'),
            'param_name' => 'bg_position_status',
            'description' => __('Voulez-vous positionner l\'image de fond ?', 'Custom'),
            "value" => true,
            'admin_label' => true
        ),
        array(
            'type' => 'textfield',
            'heading' => __('Positionnement vecteur X pour image de fond', 'Custom'),
            'param_name' => 'background_position_x',
            'description' => __('Saississez une valeur pour positionner horizontalement l\'image (en %, en px ou termes de positionnement: left, center, right )', 'Custom'),
            'dependency' => array(
                'element' => 'bg_position_status',
                'value' => 'true',
            )
        ),
        array(
            'type' => 'textfield',
            'heading' => __('Positionnement vecteur Y pour image de fond', 'Custom'),
            'param_name' => 'background_position_y',
            'description' => __('Saississez une valeur pour positionner verticalement l\'image (en %, en px ou termes de positionnement: top, center, bottom )', 'Custom'),
            'dependency' => array(
                'element' => 'bg_position_status',
                'value' => 'true',
            )
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __('Couleur de fond', 'Custom'),
            'param_name' => 'color'
        ),
        array(
            'type' => 'checkbox',
            'heading' => __('Fond sur toute la largeur', 'Custom'),
            'param_name' => 'fullwidth',
            'description' => __('Voulez-vous que le fond prenne toute la largeur ?', 'Custom'),
            "value" => true,
            'admin_label' => true
        ),
        array(
            'type' => 'checkbox',
            'heading' => __('Contenu dans le conteneur', 'Custom'),
            'param_name' => 'container',
            'description' => __('Voulez-vous du contenu dans le conteneur ?', 'Custom'),
            "value" => true,
            'admin_label' => true
        ),
        array(
            'type' => 'textfield',
            'heading' => 'Padding-top',
            'param_name' => 'pad_t',
        ),
        array(
            'type' => 'textfield',
            'heading' => 'Padding-bottom',
            'param_name' => 'pad_b',
        ),
        array(
            'type' => 'checkbox',
            'heading' => __('Ombre Superieure', 'Custom'),
            'param_name' => 'shadow_top',
            'description' => __('Voulez-vous une ombre supérieure ?', 'Custom'),
            "value" => false
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __('Couleur de l\'ombre Superieur', 'Custom'),
            'param_name' => 'color_shadow_top',
            'description' => __('Sélectionnez la couleur de l\'ombre', 'Custom'),
            'dependency' => array(
                'element' => 'shadow_top',
                'value' => 'true',
            )
        ),
        array(
            'type' => 'checkbox',
            'heading' => __('Ombre Inferieure', 'Custom'),
            'param_name' => 'shadow_bottom',
            'description' => __('Voulez-vous une ombre inférieure ?', 'Custom'),
            "value" => false
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __('Couleur de l\'ombre Inférieur', 'Custom'),
            'param_name' => 'color_shadow_bottom',
            'description' => __('Sélectionnez la couleur de l\'ombre', 'Custom'),
            'dependency' => array(
                'element' => 'shadow_bottom',
                'value' => 'true',
            )
        ),
        array(
            'type' => 'checkbox',
            'heading' => __('Bordure', 'Custom'),
            'param_name' => 'has_border',
            'description' => __('Voulez-vous une bordure?', 'Custom'),
            "value" => false
        ),
        array(
            'type' => 'textfield',
            'heading' => __('Style de la bordure', 'Custom'),
            'param_name' => 'border_style',
            'description' => __('Par exemple : "solid 1px #555" ', 'Custom'),
            'dependency' => array(
                'element' => 'has_border',
                'value' => 'true',
            )
        ),
        array(
            'type' => 'vc_link',
            'heading' => __('Lien vers une page', 'yabawt'),
            'param_name' => 'link',
            'description' => __('Lien cliquable sur toute la section', 'yabawt'),
            'admin_label' => true
        ),
        array(
            'type' => 'textfield',
            'heading' => __('Nom de la classe additionnelle', 'Custom'),
            'param_name' => 'el_class',
            'description' => __('Pour créer un style particulier pour cet élément.', 'Custom'),
        ),
    )
));

class WPBakeryShortCode_vc_section extends WPBakeryShortCodesContainer {

    protected function content($atts, $content = null) {
        extract(shortcode_atts(array(
            'background_img' => '',
            'bg_position_status' => '',
            'background_position_x' => '',
            'background_position_y' => '',
            'color' => '',
            'fullwidth' => '',
            'container' => '',
            'pad_t' => '',
            'pad_b' => '',
            'shadow_top' => '',
            'color_shadow_top' => '',
            'shadow_bottom' => '',
            'color_shadow_bottom' => '',
            'has_border' => '',
            'border_style' => '',
            'link' => '',
            'el_class' => ''
                        ), $atts));
        ob_start();

        $parent_class = 'custom-section ';

        if ($fullwidth == false) {
            $parent_class .= 'container';
        }
        //2 conditions à verifier dans la mm condition, la table de verité ne fonctionne dans ce cas que si seulement l'un des 2 cas sont vrai ou les 2.
        if ($shadow_top == true || $shadow_bottom == true) {
            $parent_class .= ' custom-section-shadow '; // Class permettant la configuration du Z-INDEX
        }
        //2 conditions à verifier dans la mm condition, la table de verité ne fonctionne dans ce cas que si seulement les des 2 cas sont vrai 
        $content_class = '';
        if ($container == true && $fullwidth == true) {
            $content_class = 'container';
        }
        $parent_style = '';

        if (!empty($background_img) && !empty($color)) { // Détermine si une variable est définie et est différente de NULL.
            $image_attributes = wp_get_attachment_image_src($background_img, 'fullscreen'); // returns an array
            $bg_url = $image_attributes[0];
            $parent_style .= 'background: ' . $color . ' url(' . $bg_url . '); ';
        } else {
            if (!empty($background_img)) { // Détermine si une variable est définie et est différente de NULL.
                $image_attributes = wp_get_attachment_image_src($background_img, 'fullscreen'); // returns an array
                $bg_url = $image_attributes[0];
                $parent_style .= 'background-image: url(' . $bg_url . '); ';
            }
            $color_bg = '';
            if (!empty($color)) {
                $color_bg = ' background-color: ' . $color . ';';
            }
        }
        
        if($bg_position_status == true){
            $parent_style .= ' background-position : '. $background_position_x . ' ' . $background_position_y . ';' ;
        }
        
        // Paramétrage de la disposition de l'ombre
        if ($shadow_top == true && $shadow_bottom == true) {
            $parent_style .= ' box-shadow: 0 4px 7px ' . $color_shadow_bottom . ',0 -2px 5px ' . $color_shadow_top . '; ';
        } else if ($shadow_top == true) {
            $parent_style .= ' box-shadow : 0 -5px 5px ' . $color_shadow_top . '; ';
        } else if ($shadow_bottom == true) {
            $parent_style .= ' box-shadow: 0 5px 5px ' . $color_shadow_bottom . '; ';
        }



        $padding_css = '';
        if (!empty($pad_t)) {
            $padding_css .= ' padding-top : ' . $pad_t . '; ';
        }
        if (!empty($pad_b)) {
            $padding_css .= ' padding-bottom : ' . $pad_b . '; ';
        }
        if ($has_border == true) {
            $parent_style .= ' border :' . $border_style . '; ';
        }
        $linkObj = '';
        if (isset($link))
            $linkObj = $link;
        $linkObj = ( '||' === $linkObj ) ? '' : $linkObj;
        $linkObj = vc_build_link($linkObj);
        $a_href = '';
        if (strlen($linkObj['url']) > 0) {
            $use_link = true;
            $a_href = $linkObj['url'];
            $a_title = $linkObj['title'];
            $a_target = $linkObj['target'];
        }
        ?>

        <div class="<?php echo $parent_class; ?>" style="<?php echo $parent_style; ?> background-repeat-y: no-repeat; ">             
            <?php if (!empty($a_href)) { ?>
                <a href="<?php echo $a_href ?>">   
                <?php } ?>
                <div class="mask" style="<?php echo $color_bg . $padding_css; ?>">                       
                    <div class="<?php echo $content_class . ' ' . $el_class; ?>">
                        <?php echo do_shortcode($content); ?> 
                    </div>
                </div>                
                <?php if (!empty($a_href)) { ?>
                </a>
            <?php } ?>
        </div> 

        <?php
        $result = ob_get_clean();
        return do_shortcode($result);
    }

}

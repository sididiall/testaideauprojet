<?php
vc_map(array(
    'name' => __('Competence'),
    'base' => 'vc_skills',
    'description' => __('Titre de Section'),
    'content_element' => true,
    'category' => 'Custom',
    'js_view' => 'VcColumnView',
    'params' => array(
        array(
            'type' => 'textfield',
            'heading' => __('Legende', 'Custom'),
            'param_name' => 'legend'
        ),
        array(
            'type' => 'textfield',
            'heading' => __('Nombre de note orange', 'Custom'),
            'param_name' => 'rateO'
        ),
        array(
            'type' => 'textfield',
            'heading' => __('Nombre de note white', 'Custom'),
            'param_name' => 'rateW'
        ),
    )
));

class WPBakeryShortCode_vc_skills extends WPBakeryShortCode {

    protected function content($atts, $content = null) {
        extract(shortcode_atts(array(
            'legend' => '',
            'rateO' => '',
            'rateW' => ''), $atts));
        ob_start();
        ?>

        <div class="skill-container">
            <span class="legend"><?php echo $input['legend']; ?></span>
            <?php for ($index = 0; $index <= $index0; $index++) { ?>
                <i class="orange"></i>
            <?php } ?>
            <?php for ($index = 0; $index <= $indexW; $index++) { ?>
                <i class="white"></i>
            <?php } ?>
        </div>

        <?php
        $result = ob_get_clean();
        return do_shortcode($result);
    }

}

<?php

class ProfileWidget extends WP_Widget {

    function __construct() {
        // Instantiate the parent object
        parent::__construct(false, "Profile");
    }

    function widget($args, $instance) {
        ob_start();
        ?>
            <div> sidebar</div>
            
        <?php
        return ob_get_clean();
    }

    function form($instance) {
        $title = !empty($instance['title']) ? $instance['title'] : esc_html__('New title', 'text_domain');
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_attr_e('Title:', 'text_domain'); ?></label> 
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>">
        </p>
        <?php
    }

    function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? sanitize_text_field($new_instance['title']) : '';

        return $instance;
    }

}

function myplugin_register_widgets() {
    register_widget('ProfileWidget');
}

add_action('widgets_init', 'myplugin_register_widgets');

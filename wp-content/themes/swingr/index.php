<?php get_header(); ?>
<div id="primary" class="content-area">
    <div id="content" role="main">
        <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
                <div class="row">
                    <div class="col-md-3">
                        <section class="sidebar-section">
                            <img class="img-profile" src="<?php e_asset_url("img/thomas"); ?>"/>
                            <div class="img-mask">
                                <div class="text"></div>
                            </div>
                            <div class="sidebar-profile-section">
                                <div class="sidebar-profile">
                                    <span>Joseph Y.</span>
                                    <span>Pouparité - Basse</span>
                                    <span></span>
                                </div>

                                <div class="sidebar-subcontent">
                                    <div class="sidebar-profile-cta">Booster ma popularité</div>
                                    <ul class="sidebar-profile-option">
                                        <li>Swinger</li>
                                        <li>Premium</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="sidebar-manage">
                                <ul class="sidebar-manage-option">
                                    <li>Gestion de profil</li>
                                    <li>Gestion des photos</li>
                                </ul>
                            </div>

                            <div class="sidebar-menu">
                                <ul class="sidebar-menu-option">
                                    <li>Visites</li>
                                    <li>Messages</li>
                                    <li>Favoris</li>
                                    <li>Admirateurs</li>
                                    <li>Matchs</li>
                                    <li>Populaires</li>
                                </ul>
                            </div>
                        </section>
                    </div> 
                    <div class="col-md-9">
                        <?php the_content(); ?>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>

    </div><!-- #content -->
    <?php get_footer(); ?>
</div><!-- #primary -->
